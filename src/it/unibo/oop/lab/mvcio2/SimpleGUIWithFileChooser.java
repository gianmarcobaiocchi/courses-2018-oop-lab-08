package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Time;
import java.util.concurrent.TimeUnit;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JWindow;

import it.unibo.oop.lab.mvcio.Controller;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {

    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */
private final JFrame frame = new JFrame("SimpleGUIWithFileChooser");
	/**
	 * Constructor.
	*/
	public SimpleGUIWithFileChooser() {
		final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setSize(((int) screen.getWidth()) / 2, ((int) screen.getHeight()) / 2);
		final JPanel mainPanel = new JPanel(new BorderLayout());
		final JPanel secondPanel = new JPanel(new BorderLayout());
		final JButton browseButton = new JButton("Browes...");
		final JTextField textField = new JTextField();
		final Controller controller = new Controller();
		textField.setEditable(false);
		secondPanel.add(textField, BorderLayout.CENTER);
		secondPanel.add(browseButton, BorderLayout.LINE_END);
		mainPanel.add(secondPanel, BorderLayout.NORTH);
		browseButton.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				final JFileChooser fileChooser = new JFileChooser();
				final int choosen = fileChooser.showSaveDialog(new JFrame());
				if (choosen == JFileChooser.APPROVE_OPTION) {
					controller.setFile(fileChooser.getSelectedFile());
					textField.setText(fileChooser.getSelectedFile().getPath());
					final JWindow messageWindow = fileChanged(textField.getText());
					try {
						TimeUnit.SECONDS.sleep(3);
					} catch (Exception exp2) {
						messageWindow.setVisible(false);
					}
				} else if (choosen == JFileChooser.CANCEL_OPTION) {
					JOptionPane.showMessageDialog(new JFrame(), "Attenzione!\nNon è stato aggiunto niente.", "errore", 0);
				}
			}
		});
		frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationByPlatform(true);
		frame.setVisible(true);
	}
	
	private JWindow fileChanged(final String file) {
//		JOptionPane.showMessageDialog(new JFrame(), "Complimenti hai cambiato il file!\n" + file, "Nuovo file", 0);
		final JWindow messageWindow = new JWindow();
		final JPanel messagePanel = new JPanel();
		messagePanel.setBackground(Color.GREEN);
		messagePanel.add(new JLabel("Grandeee!\nHai cambiato il file in " + file));
		messageWindow.setContentPane(messagePanel);
		final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		messageWindow.setSize(((int) screen.getWidth()) / 3, ((int) screen.getHeight()) / 3);
		messageWindow.setLocationByPlatform(true);
		messageWindow.setVisible(true);
		return messageWindow;
	}
	
	/**
	 * @param args
	*/
	public static void main(final String... args) {
		new SimpleGUIWithFileChooser();
	}
}
