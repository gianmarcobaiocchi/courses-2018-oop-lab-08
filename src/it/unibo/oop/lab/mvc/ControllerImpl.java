
package it.unibo.oop.lab.mvc;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.*;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 
 *
 */
public class ControllerImpl implements Controller {

    private static final String DEFAULTPATH = System.getProperty("user.home")
            + System.getProperty("file.separator")
            +  "output.txt";
    private File file;
    private String stringToPrint;
    private final List<String> olderStrings;

    /**
     * Constructor.
    */
    public ControllerImpl() {
    	this.file = new File(DEFAULTPATH);
        this.stringToPrint = null;
        this.olderStrings = new LinkedList<>();
    }

    /**
     * @param stringToPrint 
     * 		coda di stringhe da scrivere su file
	 * @throws NullPointerException
	 * 		tira l'eccezione se viene passato una stringa pari a null
	 */
	public void addStringToPrint(final String stringToPrint) throws NullPointerException {
        if (stringToPrint == null) {
        	throw new NullPointerException();
        } else {
    		this.stringToPrint = stringToPrint;
    	}
	}
	
	/**
	 * 
	 * @return String
	 */
	public String getNextStringToPrint() {
		return this.stringToPrint;
	}
	
	/**
	 * 
	 * @return List<String>
	 */
	public List<String> getHistoryStringsPrinted() {
		return this.olderStrings;
	}
	
	/**
	 * 
	 * @throws IllegalStateException se non è presente alcuna stringa da scrivere
	 */
	public void printString() throws IllegalStateException {
		if (stringToPrint == null) {
			throw new IllegalStateException();
		} else {
			this.writeFile(stringToPrint);
			this.olderStrings.add(this.stringToPrint);
			stringToPrint = null;
    	}
	}
	
	/**
	 * Metodo di settaggio del file.
	 * @param file file su cui scrivere
	*/
	public void setFile(final File file) {
		this.file = file;
	}
	
	/**
	 * Metodo di ritorno del file.
	 * @return File
	*/
	public File getFile() {
		return this.file;
	}
	
	/**
	 * Metodo di ritorno il path del file.
	 * @return String
	*/
	public String getPath() {
		return this.file.getPath();
	}
	
	/**
	 * Metodo che scrive una stringa nel file.
	 * @param string stringa da scrivere su file
	*/
	public void writeFile(final String string) {
		try (PrintStream ps = new PrintStream(file)) {
			ps.println(string);
		} catch (IOException e) {
			System.out.println("IOException: problemi di I/O!");
		}
	}
}
